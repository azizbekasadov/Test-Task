//
//  ManagerPurchase.swift
//  Picsart Luxury
//
//  Created by Alex Balukhtsin on 9.10.22.
//  Copyright © 2022 Tribal Media House. All rights reserved.
//

import Foundation
import StoreKit

enum StatePurchase: String {
    case failed = "transaction failed"
    case purchased = "transaction ok"
}

class ManagerPurchase: NSObject {
    
    static let productIdentifier = "IAPManagerProductIdentifier"
    static let shared = ManagerPurchase()
    
    private override init() {}
    
    var products: [SKProduct] = []
    let paymentQueue = SKPaymentQueue.default()
    var statePurchsed: StatePurchase = .purchased
    
    public func setupPurchases(callback: @escaping(Bool) -> ()) {
        if SKPaymentQueue.canMakePayments() {
            paymentQueue.add(self)
            callback(true)
            return
        }
        
        callback(false)
    }
    
    public func getProducts() {
        let identifiers: Set = [
            IAPProducts.weeklySub.rawValue]
        
        let productRequest = SKProductsRequest(productIdentifiers: identifiers)
        productRequest.delegate = self
        productRequest.start()
    }
    
    public func purchase(productWith identifier: String) {
        guard let product = products.filter({ $0.productIdentifier == identifier }).first else { return }
        
        let payment = SKPayment(product: product)
        paymentQueue.add(payment)
    }
    
    public func restoreCompletedTransactions() {
        paymentQueue.restoreCompletedTransactions()
    }
    
}

extension ManagerPurchase: SKPaymentTransactionObserver {
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch transaction.transactionState {
            case .deferred:
                break
            case .purchasing:
                break
            case .failed:
                statePurchsed = .failed
                failed(transaction: transaction)
            case .purchased :
                statePurchsed = .purchased
                completed(transaction: transaction)
            case .restored:
                restored(transaction: transaction)
            }
        }
    }
    
    private func failed(transaction: SKPaymentTransaction) {
        if let transactionError = transaction.error as NSError? {
            if transactionError.code != SKError.paymentCancelled.rawValue {
                print("Ошибка транзакции: \(transaction.error!.localizedDescription)")
            }
        }
        NotificationCenter.default.post(name: NSNotification.Name("statePurchsed"), object: statePurchsed)
        NotificationCenter.default.post(name: NSNotification.Name("failedPurchsed"), object: statePurchsed)
        paymentQueue.finishTransaction(transaction)
    }
    
    private func completed(transaction: SKPaymentTransaction) {
//        let receiptValidator = ReceiptValidator()
//        let result = receiptValidator.validateReceipt()
//
//        switch result {
//        case let .success(parsedReceipt):
//            UserDefaults.standard.set(true, forKey: "wasPurchase")
//            NotificationCenter.default.post(name: NSNotification.Name(transaction.payment.productIdentifier), object: nil)
//            paymentQueue.finishTransaction(transaction)
//        case .error(let receiptValidationError):
//            NotificationCenter.default.post(name: NSNotification.Name(transaction.payment.productIdentifier), object: nil)
//            print(receiptValidationError.localizedDescription)
//        }
//        paymentQueue.finishTransaction(transaction)
       
    }
    
    private func restored(transaction: SKPaymentTransaction) {
        
        NotificationCenter.default.post(name: NSNotification.Name(transaction.payment.productIdentifier), object: nil)
        paymentQueue.finishTransaction(transaction)
    }
}

extension ManagerPurchase: SKProductsRequestDelegate {
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        self.products = response.products
        products.forEach {
            print($0.localizedTitle)
        }
        
        if products.count > 0 {
            NotificationCenter.default.post(name: NSNotification.Name( ManagerPurchase.productIdentifier), object: nil)
            
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    NotificationCenter.default.post(name: NSNotification.Name("endLoadingPurchases"), object: nil)
                }
            
           // NotificationCenter.default.post(name: NSNotification.Name("endLoadingPurchases"), object: nil)
        }
    }
    
    func isICloudContainerAvailable() -> Bool {
        if let _ = FileManager.default.ubiquityIdentityToken {
                return true
        } else {
            return false
        }
    }
}
