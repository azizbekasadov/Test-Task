//
//  IAPManager.swift
//  Picsart Luxury
//
//  Created by Alex Balukhtsin on 8.10.22.
//  Copyright © 2022 Tribal Media House. All rights reserved.
//

import StoreKit
import SwiftyStoreKit
import UIKit
import UserNotifications

let PURCHASE_MANAGER = PurchaseManager.shared

typealias PurchaseHandler = (_ transactionId: String?, _ success: Bool, _ error: Error?) -> ()

class PurchaseManager: NSObject {
    
    static var shared = PurchaseManager()
    
     var subscriptionsIds: Set<String> {
         Set([
            "com.tsamimi.PiscartLuxury.weekly"])
    }
    
    private var bundleID: String {
        Bundle.main.bundleIdentifier ?? "error"
    }
    
    func sunbscriptionId(name: String) -> String {
         bundleID + "." + name
    }
    
    var expiredDate: Date? {
        get {
            UserDefaults.standard.object(forKey: "expiredDate") as? Date
        } set {
            UserDefaults.standard.set(newValue, forKey: "expiredDate")
            UserDefaults.standard.synchronize()
        }
    }
    
    var isExpired: Bool {
//        return false
//        #if DEBUG
//        if UserDefaults.standard.bool(forKey: "supervip") {
//            return false
//        }
//        #else
//        #endif
        guard let expiredDate = expiredDate else {
            return true
        }
        return Date() > expiredDate
    }
    
    var products: [SKProduct]? {
        didSet {
            products?.forEach { product in
                UserDefaults.standard.set("\(product.priceLocale.currencySymbol ?? product.priceLocale.currencyCode ?? "")\(product.price)", forKey: product.productIdentifier)
                
            }
            UserDefaults.standard.synchronize()
        }
    }
    
    var productsInfo: [String: String]? {
            var info: [String: String] = [:]
            subscriptionsIds.forEach({ purchaseId in
                if let priceValue = UserDefaults.standard.string(forKey: purchaseId) {
                    info[purchaseId] = priceValue
                }
            })
            return info.values.count > 0 ? info : nil
        
    }
    
    func getProductsInfo(completion: (([SKProduct]?, Error?) -> ())?) {
        guard (products?.count ?? 0) == 0 else {
            completion?(products, nil)
            return
        }
        
        SwiftyStoreKit.retrieveProductsInfo(subscriptionsIds) { result in
            if result.retrievedProducts.count > 0 {
                self.products = Array(result.retrievedProducts)
            } else {
                self.products = nil
            }
            self.products?.sort { product1, product2 -> Bool in
                product1.price.doubleValue < product2.price.doubleValue
            }
            completion?(self.products, result.error)
            self.verifyReceipt(purchase: nil, completion: nil)
        }
    }
    
    func purchase(productId: String, completion: PurchaseHandler?) {
        let handler: (PurchaseResult) -> () = { result in
            switch result {
            case .success(let purchase):
                UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
                if purchase.needsFinishTransaction {
                    SwiftyStoreKit.finishTransaction(purchase.transaction)
                }
                
                self.verifyReceipt(purchase: purchase, completion: completion)
                DispatchQueue.global(qos: .background).async {
                    self.sendEventPurchase(purchase: purchase)
                }
            case .error(let error):
                switch error.code {
                case .paymentCancelled:
                    print("error")
                default:
                    print("error")
                }
                
//                if error.code == .unknown {
//                    self.purchase(productId: productId, completion: completion)
//                    return
//                }
                completion?(nil, false, error)
            }
        }
        
        if let product = PURCHASE_MANAGER.products?.first(where: { $0.productIdentifier == productId }) {
            SwiftyStoreKit.purchaseProduct(product, completion: handler)
        } else {
            SwiftyStoreKit.purchaseProduct(productId, completion: handler)
        }
    }
    
    private func sendEventPurchase(purchase: PurchaseDetails?) {
        let sharedSecret = AppDelegate.shared.secretCode
        
        #if DEBUG
        let environment = AppleReceiptValidator.VerifyReceiptURLType.sandbox
        #else
        let environment = AppleReceiptValidator.VerifyReceiptURLType.production
        #endif
 
        let appleValidator = AppleReceiptValidator(service: environment, sharedSecret: sharedSecret)
        SwiftyStoreKit.verifyReceipt(using: appleValidator) { result in
            switch result {
            case .success(let receipt):
                // Verify the purchase of a Subscription
                self.products?.forEach { product in
                    let purchaseResult = SwiftyStoreKit.verifySubscription(
                        ofType: .autoRenewable,
                        productId: product.productIdentifier,
                        inReceipt: receipt)
                    switch purchaseResult {
                    case let .purchased(expiryDate, items):
                        guard let receiptData = SwiftyStoreKit.localReceiptData else { return }
                        let _ = receiptData.base64EncodedString(options: [])
                        
                        print("\(product.productIdentifier) is valid until \(expiryDate)\n\(items)\n")
                        
                    default:
                        print("not purchased")
                    }
                }
 
            case .error(let error):
                print("Receipt verification failed: \(error)")
     
            }
        }
         
    }

    func completeTransactions() {
        SwiftyStoreKit.completeTransactions(atomically: true) { purchases in
            for purchase in purchases {
                switch purchase.transaction.transactionState {
                case .purchased, .restored:
                    if purchase.needsFinishTransaction {
                        // Deliver content from server, then:
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                    }
                // Unlock content
                case .failed, .purchasing, .deferred:
                    break // do nothing
                @unknown default:
                    break
                }
            }
        }
    }
    
    func verifyReceipt(purchase: PurchaseDetails?, completion: PurchaseHandler?) {
        #if DEBUG
        let environment = AppleReceiptValidator.VerifyReceiptURLType.sandbox
        #else
        let environment = AppleReceiptValidator.VerifyReceiptURLType.production
        #endif
        let sharedSecret = AppDelegate.shared.secretCode
        guard sharedSecret != "" else {
            let error = NSError(domain: "verifyReceipt", code: 0, userInfo: ["message": "empty sharedSecret"])
            
            completion?(nil, false, error)
            return
        }
        
        let appleValidator = AppleReceiptValidator(service: environment, sharedSecret: sharedSecret)
        SwiftyStoreKit.verifyReceipt(using: appleValidator) { result in
            switch result {
            case .success(let receipt):
                // Verify the purchase of a Subscription
                var expiredDates: [Date] = []
                
                self.products?.forEach { product in
                    
                        let purchaseResult = SwiftyStoreKit.verifySubscription(
                                                ofType: .autoRenewable,
                                                productId: product.productIdentifier,
                                                inReceipt: receipt)
                        switch purchaseResult {
                        case let .purchased(expiryDate, items):
                            print("\(product.productIdentifier) is valid until \(expiryDate)\n\(items)\n")
                            expiredDates.append(expiryDate)
                        case let .expired(expiryDate, items):
                            print("\(product.productIdentifier) is expired since \(expiryDate)\n\(items)\n")
                            expiredDates.append(expiryDate)
                        case .notPurchased:
                            print("The user has never purchased \(product.productIdentifier)")
                        }
                }
                
                expiredDates.sort(by: { date1, date2 -> Bool in
                     date1.timeIntervalSince1970 >= date2.timeIntervalSince1970
                })
                self.expiredDate = expiredDates.first
                completion?(purchase?.transaction.transactionIdentifier, !self.isExpired, nil)
            case let .error(error):
                print("Receipt verification failed: \(error)")
                completion?(nil, false, error)
            }
        }
    }
    
    func restore(completion: PurchaseHandler?) {
        SwiftyStoreKit.restorePurchases(atomically: true) { results in
            if results.restoreFailedPurchases.count > 0 {
                print("Restore Failed: \(results.restoreFailedPurchases)")
                completion?(nil, false, results.restoreFailedPurchases.first?.0)
            } else if results.restoredPurchases.count > 0 {
                print("Restore Success: \(results.restoredPurchases)")
                self.verifyReceipt(purchase: nil, completion: completion)
            } else {
                completion?(nil, true, nil)
            }
        }
    }
    
}

