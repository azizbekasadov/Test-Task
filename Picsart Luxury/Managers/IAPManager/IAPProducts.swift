//
//  IAPProducts.swift
//  Picsart Luxury
//
//  Created by Alex Balukhtsin on 8.10.22.
//  Copyright © 2022 Tribal Media House. All rights reserved.
//

import Foundation

enum IAPProducts: String {
    case weeklySub = "com.tsamimi.PiscartLuxury.weekly"
}
