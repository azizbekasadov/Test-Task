//
//  YandexMetrica.swift
//  Picsart Luxury
//
//  Created by Zhanibek on 03.10.2022.
//  Copyright © 2022 Tribal Media House. All rights reserved.
//

import Foundation
import YandexMobileMetrica

@objc class YandexMetrica: NSObject {
    
    @objc static func sendScreenView(eventType: String) {
        let param =  ["screen_type": eventType]
        YMMYandexMetrica.reportEvent(eventType, parameters: param, onFailure: nil)
    }
}
