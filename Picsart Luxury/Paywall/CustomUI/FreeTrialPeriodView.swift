//
//  FreeTrialPeriodView.swift
//  Picsart Luxury
//
//  Created by Антон Титов on 29/9/22.
//  Copyright © 2022 Tribal Media House. All rights reserved.
//

import UIKit
import SnapKit

final class FreeTrialPeriodView: UIView {
    
    var onSwitchState: (() -> Void)?
    var offSwitchState: (() -> Void)?
    
    private var questionLabel: UILabel = {
        let label = UILabel()
        label.text =  "Any doubts?"
        label.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
//        label.font = UIFont(name: "Poppins-Medium", size: 14)
        label.font = UIFont.systemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private var trialPeriodLabel: UILabel = {
        let label = UILabel()
        label.text =  "Start free trial" 
        label.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
//        label.font = UIFont(name: "Poppins-Medium", size: 18)
        label.font = UIFont.systemFont(ofSize: 18)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private var freeTrialPeriodSwitch: UISwitch = {
        let switchOn = UISwitch()
        switchOn.thumbTintColor = .black
        switchOn.onTintColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
        switchOn.translatesAutoresizingMaskIntoConstraints = false
        
        return switchOn
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupConstraints()
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupConstraints() {
        addSubview(questionLabel)
        
        questionLabel.snp.makeConstraints { make in
            make.top.leading.equalToSuperview()
        }
        
        addSubview(trialPeriodLabel)
        
        trialPeriodLabel.snp.makeConstraints { make in
            make.top.equalTo(questionLabel.snp.bottom).offset(8)
            make.leading.bottom.equalToSuperview()
        }
        
        addSubview(freeTrialPeriodSwitch)
        
        freeTrialPeriodSwitch.snp.makeConstraints { make in
            make.leading.equalTo(trialPeriodLabel.snp.trailing).offset(47)
            make.trailing.bottom.equalToSuperview()
            make.width.equalTo(51)
            make.height.equalTo(31)
        }
    }
    
    private func setupUI() {
        freeTrialPeriodSwitch.setOn(false, animated: true)
        freeTrialPeriodSwitch.addTarget(self, action: #selector(switchPressed), for: .allEvents)
    }
    
    @objc private func switchPressed() {
        if freeTrialPeriodSwitch.isOn {
            questionLabel.isHidden = true
            trialPeriodLabel.text = NSLocalizedString("Try it for free!", comment: "")
            onSwitchState?()
        } else {
            questionLabel.isHidden = false
            trialPeriodLabel.text = NSLocalizedString("Start free trial", comment: "")
            offSwitchState?()
        }
    }
}
