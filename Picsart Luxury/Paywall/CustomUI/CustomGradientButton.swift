//
//  CustomGradientButton.swift
//  Picsart Luxury
//
//  Created by Антон Титов on 30/9/22.
//  Copyright © 2022 Tribal Media House. All rights reserved.
//

import UIKit

final class CustomGradientButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupGradientLayer()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupGradientLayer() {
        let gradient = CAGradientLayer()
        let colors = [UIColor(red: 0.039, green: 0.643, blue: 0.878, alpha: 1).cgColor,
                      UIColor(red: 0.34, green: 0.355, blue: 0.566, alpha: 1).cgColor,
                      UIColor(red: 0.792, green: 0.224, blue: 0.482, alpha: 1).cgColor]
        
        gradient.colors = colors
        gradient.locations = [0, 0.5, 1]
        gradient.startPoint = CGPoint(x: 0.25, y: 0.5)
        gradient.endPoint = CGPoint(x: 0.75, y: 0.5)
        gradient.frame = self.bounds
        gradient.cornerRadius = 8
        self.layer.insertSublayer(gradient, at: 0)
    }
}
