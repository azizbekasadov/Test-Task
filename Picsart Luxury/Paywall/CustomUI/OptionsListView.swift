//
//  OptionsListView.swift
//  Picsart Luxury
//
//  Created by Антон Титов on 28/9/22.
//  Copyright © 2022 Tribal Media House. All rights reserved.
//

import UIKit
import SnapKit

final class OptionsListView: UIView {
    
    private var optionsListStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 15
        stackView.alignment = .leading
        
        return stackView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupConstraints()
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupConstraints() {
        addSubview(optionsListStackView)
        
        optionsListStackView.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
        }
    }
    
    private func setupUI() {
        let cellFirst = OptionCellView()
        let cellSecond = OptionCellView()
        let cellThird = OptionCellView()
        let cellFourth = OptionCellView()
        
        cellFirst.setupUI(textLabel: "Free 50000+ quotes")
        cellSecond.setupUI(textLabel: "Hundreds of social media templates")
        cellThird.setupUI(textLabel: "Meme generator")
        cellFourth.setupUI(textLabel: "Unique fonts, stickers, \n filters and effects")
        
        optionsListStackView.addArrangedSubview(cellFirst)
        optionsListStackView.addArrangedSubview(cellSecond)
        optionsListStackView.addArrangedSubview(cellThird)
        optionsListStackView.addArrangedSubview(cellFourth)
    }
}
