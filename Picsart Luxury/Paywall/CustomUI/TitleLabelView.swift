//
//  TitleLabelView.swift
//  Picsart Luxury
//
//  Created by Антон Титов on 28/9/22.
//  Copyright © 2022 Tribal Media House. All rights reserved.
//

import UIKit
import SnapKit

final class TitleLabelView: UIView {
    
    private var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 35)
//        label.font = UIFont(name: "Poppins-Medium", size: 40)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private var titleImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "paywallTitleImage")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupConstraints() {
        addSubview(titleLabel)
        
        titleLabel.snp.makeConstraints { make in
            make.top.leading.trailing.equalToSuperview()
        }
        
        addSubview(titleImage)
        
        titleImage.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(8)
            make.bottom.centerX.equalToSuperview()
            make.width.equalTo(titleLabel).offset(-70)
            make.height.equalTo(13.6)
        }
    }
    
    func setupUI(textLabel: String) {
        titleLabel.text = textLabel
    }
}
