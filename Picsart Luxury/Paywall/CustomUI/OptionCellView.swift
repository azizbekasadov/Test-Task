//
//  OptionCellView.swift
//  Picsart Luxury
//
//  Created by Антон Титов on 28/9/22.
//  Copyright © 2022 Tribal Media House. All rights reserved.
//

import UIKit
import SnapKit

final class OptionCellView: UIView {
    
    private var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .center
        imageView.image = UIImage(named: "paywallDone_icon")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        return imageView
    }()
    
    private var optionLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
//        label.font = UIFont(name: "Poppins-Medium", size: 16)
        label.font = UIFont.systemFont(ofSize: 18)
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupConstraints() {
        addSubview(iconImageView)
        
        iconImageView.snp.makeConstraints { make in
            make.leading.top.equalToSuperview()
            make.width.height.equalTo(18)
        }
        
        addSubview(optionLabel)
        
        optionLabel.snp.makeConstraints { make in
            make.leading.equalTo(iconImageView.snp.trailing).offset(8)
            make.top.trailing.bottom.equalToSuperview()
        }
    }
    
    func setupUI(textLabel: String) {
        optionLabel.text = textLabel
    }
}
