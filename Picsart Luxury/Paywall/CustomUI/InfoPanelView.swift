//
//  InfoPanelView.swift
//  Picsart Luxury
//
//  Created by Антон Титов on 30/9/22.
//  Copyright © 2022 Tribal Media House. All rights reserved.
//

import UIKit
import SnapKit

final class InfoPanelView: UIView {
    
    // MARK: - Properties
    
    var restorePurchasesAction: (() -> Void)?
    var termsOfServiceAction: (() -> Void)?
    var privacyPolicyAction: (() -> Void)?
    
    // MARK: - Private Properties

    private let restorePurchasesLabel: UILabel = {
        let label = UILabel()
        label.text = "Restore Purchases"
        label.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
//        label.font = UIFont(name: "Poppins-Medium", size: 9)
        label.font = UIFont.systemFont(ofSize: 9)
        label.numberOfLines = 0
        label.isUserInteractionEnabled = true
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private let termsOfServiceLabel: UILabel = {
        let label = UILabel()
        label.text = "Terms of Service"
        label.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
//        label.font = UIFont(name: "Poppins-Medium", size: 9)
        label.font = UIFont.systemFont(ofSize: 9)
        label.numberOfLines = 0
        label.isUserInteractionEnabled = true
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private let privacyPolicyLabel: UILabel = {
        let label = UILabel()
        label.text = "Privacy Policy"
        label.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
//        label.font = UIFont(name: "Poppins-Medium", size: 9)
        label.font = UIFont.systemFont(ofSize: 9)
        label.textAlignment = .center
        label.numberOfLines = 0
        label.isUserInteractionEnabled = true
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private let firstDividiengLineImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "dividiengLineImage")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        return imageView
    }()
    
    private let secondDividiengLineImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "dividiengLineImage")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        return imageView
    }()
    
    // MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupConstraints()
        setupActions()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private Methods
    
    private func setupConstraints() {
        addSubview(restorePurchasesLabel)
        
        restorePurchasesLabel.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
        }
        
        addSubview(firstDividiengLineImage)
        
        firstDividiengLineImage.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.leading.equalTo(restorePurchasesLabel.snp.trailing).offset(4)
        }
        
        addSubview(termsOfServiceLabel)
        
        termsOfServiceLabel.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.centerX.equalToSuperview()
            make.leading.equalTo(firstDividiengLineImage.snp.trailing).offset(4)
        }
        
        addSubview(secondDividiengLineImage)
        
        secondDividiengLineImage.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.leading.equalTo(termsOfServiceLabel.snp.trailing).offset(4)
        }
        
        addSubview(privacyPolicyLabel)
        
        privacyPolicyLabel.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.leading.equalTo(secondDividiengLineImage.snp.trailing).offset(4)
        }
    }
    
    private func setupActions() {
        let restorePurchasesGesture = UITapGestureRecognizer(target: self, action: #selector(restorePurchasesPress))
        restorePurchasesLabel.addGestureRecognizer(restorePurchasesGesture)
        
        let termsOfServiceGesture = UITapGestureRecognizer(target: self, action: #selector(termsOfServicePress))
        termsOfServiceLabel.addGestureRecognizer(termsOfServiceGesture)
        
        let privacyPolicyGesture = UITapGestureRecognizer(target: self, action: #selector(privacyPolicyPress))
        privacyPolicyLabel.addGestureRecognizer(privacyPolicyGesture)
    }
    
    @objc private func restorePurchasesPress() {
        restorePurchasesAction?()
    }
    
    @objc private func termsOfServicePress() {
        termsOfServiceAction?()
    }
    
    @objc private func privacyPolicyPress() {
        privacyPolicyAction?()
    }
}
