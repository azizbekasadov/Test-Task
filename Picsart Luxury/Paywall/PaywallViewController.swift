//
//  PaywallViewController.swift
//  Picsart Luxury
//
//  Created by Антон Титов on 28/9/22.
//  Copyright © 2022 Tribal Media House. All rights reserved.
//

import UIKit
import SnapKit
import AVKit
import AVFoundation

final class PaywallViewController: UIViewController, ActivityIndicatorShowable {
    
    // MARK: - Private propeprties
    
    private let backImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "paywallBackImage")
        imageView.contentMode = .scaleToFill
        imageView.isUserInteractionEnabled = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        return imageView
    }()
    
    private let titleLabel: TitleLabelView = {
        let view = TitleLabelView()
        view.setupUI(textLabel: "Premium Plus")
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private let optionsListView: OptionsListView = {
        let view = OptionsListView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private let freeTrialPeriodView: FreeTrialPeriodView = {
        let view = FreeTrialPeriodView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private let nextButton: CustomGradientButton = {
        let button = CustomGradientButton()
        button.setTitle("PROCEED", for: .normal)
        button.setTitleColor(UIColor(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 16)
//        button.titleLabel?.font = UIFont(name: "Poppins-Bold", size: 16)
        button.backgroundColor = .clear
        button.translatesAutoresizingMaskIntoConstraints = false
        
        return button
    }()
    
    private   let closeButton: UIButton = {
        let button = UIButton()
        button.setBackgroundImage(UIImage.init(named: "close.png"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints=false
        button.titleLabel?.font = UIFont(name: "Poppins-Bold", size: 16)
        button.setTitle("X", for: .normal)
        return button
    }()
    
    
    private let priceLabel: UILabel = {
        let label = UILabel()
        label.text = "$7 per week"
        label.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
//        label.font = UIFont(name: "Poppins-Medium", size: 14)
        label.font = UIFont.systemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private let infoPanelView: InfoPanelView = {
        let view = InfoPanelView()
        view.isUserInteractionEnabled = true
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    // MARK: - View Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupConstraints()
        setupActions()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        setupUI()
    }
    
    // MARK: = Private methods
    
    private func setupConstraints() {
        view.addSubview(backImageView)
        
        backImageView.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
        }
        
        backImageView.addSubview(titleLabel)
        
        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(50)
            make.leading.equalToSuperview().offset(76)
            make.trailing.equalToSuperview().offset(-76)
        }
        
        backImageView.addSubview(optionsListView)
        
        optionsListView.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(50)
            make.leading.equalToSuperview().offset(60)
            make.trailing.equalToSuperview().offset(-60)
            make.centerX.equalToSuperview()
        }
        
        backImageView.addSubview(freeTrialPeriodView)
        
        freeTrialPeriodView.snp.makeConstraints { make in
            make.top.equalTo(optionsListView.snp.bottom).offset(56)
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-32)
        }
        
        backImageView.addSubview(infoPanelView)
        
        infoPanelView.snp.makeConstraints { make in
            make.bottom.equalToSuperview().offset(-24)
            make.leading.equalToSuperview().offset(10)
            make.trailing.equalToSuperview().offset(-10)
        }
        
        backImageView.addSubview(nextButton)
        
        nextButton.snp.makeConstraints { make in
            make.bottom.equalTo(infoPanelView.snp.bottom).inset(25)
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
            make.height.equalTo(56)
        }
        
       
        
        backImageView.addSubview(priceLabel)
        
        priceLabel.snp.makeConstraints { make in
            make.top.equalTo(freeTrialPeriodView.snp.bottom).offset(20)
            make.centerX.equalToSuperview()
        }
        
        backImageView.addSubview(closeButton)
        
        closeButton.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(50)//20
            make.leading.equalToSuperview().offset(20)
            make.height.equalTo(30)
            make.width.equalTo(30)
        }
    }
    
    private func setupUI() {
        nextButton.addTarget(self, action: #selector(nextButtonActions), for: .touchUpInside)
        closeButton.addTarget(self, action: #selector(closeButtonPressed), for: .touchUpInside)
        let gradient = CAGradientLayer()
        let colors = [UIColor(red: 0.039, green: 0.643, blue: 0.878, alpha: 1).cgColor,
                      UIColor(red: 0.34, green: 0.355, blue: 0.566, alpha: 1).cgColor,
                      UIColor(red: 0.792, green: 0.224, blue: 0.482, alpha: 1).cgColor]
        
        gradient.colors = colors
        gradient.locations = [0, 0.5, 1]
        gradient.startPoint = CGPoint(x: 0.25, y: 0.5)
        gradient.endPoint = CGPoint(x: 0.75, y: 0.5)
        gradient.frame = nextButton.bounds
        gradient.cornerRadius = 8
        nextButton.layer.insertSublayer(gradient, at: 0)
    }
    
    private func setupActions() {
        freeTrialPeriodView.onSwitchState = { [weak self] in
            guard let self = self else { return }
            self.nextButton.setTitle("START A FREE TRIAL", for: .normal)
            self.priceLabel.text = "$7 per week after 3 trial days"
        }
        
        freeTrialPeriodView.offSwitchState = { [weak self] in
            guard let self = self else { return }
            self.nextButton.setTitle("PROCEED", for: .normal)
            self.priceLabel.text = "$7 per week"
        }
        
        infoPanelView.termsOfServiceAction = {
            UIApplication.shared.openURL(URL(string: "https://cutt.ly/qH098fF")!)
        }
        
        infoPanelView.privacyPolicyAction = {
            UIApplication.shared.openURL(URL(string: "https://cutt.ly/XH03ufv")!)
        }
        
        infoPanelView.restorePurchasesAction = {
            self.showCustomActivityIndicator()
                PURCHASE_MANAGER.restore { (_: String?, success, _) in
                    if success == true {
                        let alert = UIAlertController(title: "Success!",
                                                      message: "All your purchases have been restored successfully",
                                                      preferredStyle: .alert)
                        let ok = UIAlertAction(title: "Ok", style: .cancel) { [weak self] _ in
                            self?.presentNextScreen()
                        }
                        alert.addAction(ok)
                        self.present(alert, animated: true, completion: nil)
                    } else {
                        let alert = UIAlertController(title: "Sorry",
                                                      message: "No active subscription or purchase found",
                                                      preferredStyle: .alert)
                        let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                        alert.addAction(ok)
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                
            }
        }
    }
    
    @objc private func nextButtonActions() {
        showCustomActivityIndicator()
        PURCHASE_MANAGER.purchase(productId: "\(IAPProducts.weeklySub.rawValue)") { [weak self] _, _, error in
            if error == nil {
                self?.hideActivityIndicator()
                self?.presentNextScreen()
            } else {
                self?.hideActivityIndicator()
                self?.presentError(error: error?.localizedDescription ?? "")
            }
        }
        //performSegue(withIdentifier: "toQuoteCategoryTableViewController", sender: nil)
    }
    
    private func presentError(error: String) {
        let alert = UIAlertController(title: "Error",
                                      message: error,
                                      preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    
    private func presentNextScreen() {
        performSegue(withIdentifier: "toQuoteCategoryTableViewController", sender: nil)
    }
    
    @objc func closeButtonPressed() {
       
        performSegue(withIdentifier: "toQuoteCategoryTableViewController", sender: nil)
        
    }
}
