//
//  RateManager.swift
//  Picsart Luxury
//
//  Created by Azizbek Asadov on 14/10/22.
//  Copyright © 2022 Tribal Media House. All rights reserved.
//

import StoreKit
import os.log

final class RateManager {
    static func rateApp() {
        if AppPreferences.shared.numberOfTimesRateHasShown < 2 {
            AppPreferences.shared.numberOfTimesRateHasShown += 1
            
            if #available(iOS 14.0, *) {
                SKStoreReviewController.requestReviewInCurrentScene()
            } else {
                SKStoreReviewController.requestReview()
            }
        } else {
            os_log("%s: %s", String(#function), "Rate limit has been reached")
        }
    }
}

extension SKStoreReviewController {
    public static func requestReviewInCurrentScene() {
        if #available(iOS 14.0, *) {
            if let scene = UIApplication.shared
                .connectedScenes
                .first(
                    where: { $0.activationState == .foregroundActive }
                ) as? UIWindowScene
            {
                requestReview(in: scene)
            }
        } else {
            requestReview()
        }
    }
}
