//
//  AppPreferences.swift
//  Picsart Luxury
//
//  Created by Azizbek Asadov on 14/10/22.
//  Copyright © 2022 Tribal Media House. All rights reserved.
//

import Foundation
import os.log

public final class AppPreferences {
    private let suiteName = "group.\(String(describing: Bundle.main.bundleIdentifier))"

    public static let shared = AppPreferences()

    private let userDefaults: UserDefaults
        
    private init() {
        userDefaults = UserDefaults(suiteName: suiteName)!
        userDefaults.register(
            defaults: [
                Key.isRated.rawValue: false,
                Key.numberOfTimesRateHasShown.rawValue: 0
            ]
        )
    }
    
    public var isRated: Bool {
        get {
            return userDefaults.bool(forKey: Key.isRated.rawValue)
        } set {
            userDefaults.set(newValue, forKey: Key.isRated.rawValue)
            os_log("Value for key %s: %s", Key.isRated.rawValue, String(describing: newValue))
        }
    }
    
    public var numberOfTimesRateHasShown: Int {
        get {
            userDefaults.integer(forKey: Key.numberOfTimesRateHasShown.rawValue)
        } set {
            userDefaults.set(newValue, forKey: Key.numberOfTimesRateHasShown.rawValue)
            os_log("Value for key %s: %s", Key.numberOfTimesRateHasShown.rawValue, String(describing: newValue))
        }
    }
}

extension AppPreferences {
    enum Key: String {
        case isRated = "APP-PREFERENCES-IS-RATED"
        case numberOfTimesRateHasShown = "APP-PREFERENCES-NUMBER-RATES"
    }
}
