//
//  IntroDataModel.swift
//  Picsart Luxury
//
//  Created by Антон Титов on 27/9/22.
//  Copyright © 2022 Tribal Media House. All rights reserved.
//

import Foundation

struct IntroDataModel {
    var slideNumber: Int
    var backImageNameView: String
    var titleLabel: String
    var infoLabel: String
    
    init(slideNumber: Int, backImageNameView: String, titleLabel: String, infoLabel: String) {
        self.slideNumber = slideNumber
        self.backImageNameView = backImageNameView
        self.titleLabel = titleLabel
        self.infoLabel = infoLabel
    }
}
