//
//  IntroPresenter.swift
//  Picsart Luxury
//
//  Created by Антон Титов on 27/9/22.
//  Copyright © 2022 Tribal Media House. All rights reserved.
//

import Foundation


protocol IntroViewInput {
    
}

protocol IntroViewOutput {
    
    func setupContent(slideNumber: Int) -> IntroDataModel?
}

final class IntroPresenter: IntroViewOutput {
    
   var content = [IntroDataModel(slideNumber: 0,
                                  backImageNameView: "NewIntro_1",
                                  titleLabel: "Meet Picsart Luxury",
                                  infoLabel: "Universal editor 3 in 1"),
                   IntroDataModel(slideNumber: 1,
                                  backImageNameView: "NewIntro_2",
                                  titleLabel: "Show your creativity",
                                  infoLabel: "Do digital art!"),
                   IntroDataModel(slideNumber: 2,
                                  backImageNameView: "NewIntro_3",
                                  titleLabel: "Perfect selfie in one moment",
                                  infoLabel: "Instantly using artificial intelligence"),
                   IntroDataModel(slideNumber: 3,
                                  backImageNameView: "NewIntro_4",
                                  titleLabel: "Unleash your creative potential",
                        infoLabel: NSLocalizedString("Turn a photo into digital art",
                                                               comment: "introInfoLabel_3")),
                   IntroDataModel(slideNumber: 4,
                                  backImageNameView: "NewIntro_5",
                                  titleLabel: "Unique filters",
                                  infoLabel: "New features every month"),
                   IntroDataModel(slideNumber: 5,
                                  backImageNameView: "NewIntro_6",
                                  titleLabel: "Not like context?",
                                  infoLabel: "100+ different backgrounds"),
   
                  IntroDataModel(slideNumber: 6,
                                 backImageNameView: "NewIntro_7",
                                 titleLabel: "Purchase now",
                                 infoLabel: "Try all chips and tools!")]
                  
    func setupContent(slideNumber: Int) -> IntroDataModel? {
        return content[slideNumber]
    }
}
