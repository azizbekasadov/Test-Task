//
//  IntroViewController.swift
//  Picsart Luxury
//
//  Created by Антон Титов on 27/9/22.
//  Copyright © 2022 Tribal Media House. All rights reserved.
//

import UIKit
import SnapKit

final class IntroViewController: UIViewController {
    
    // MARK: - Private properties
    
    private var presenter = IntroPresenter()
    private var slideNumber = 0
    
    private var viewDrag: UIView = {
       let view = UIView()
       view.backgroundColor = .white
       return view
    }()
    var panGesture       = UIPanGestureRecognizer()
    
    private var backImageView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleToFill
        image.isUserInteractionEnabled = true
        image.translatesAutoresizingMaskIntoConstraints = false
        
        return image
    }()
    
    private var backImageViewEffect: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = 0.2
        return view
    }()
    
    private let titleImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        return imageView
    }()
    
    private var titleInfoLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = .clear
        label.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
        //        label.font = UIFont(name: "Roboto-Bold", size: 40)
        label.font = UIFont.systemFont(ofSize: 36)
        label.textAlignment = .left
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private var infoLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = .clear
        label.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
        //        label.font = UIFont(name: "Roboto-Regular", size: 24)
        label.font = UIFont.systemFont(ofSize: 24)
        label.textAlignment = .left
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private var nextButton: UIButton = {
        let button = UIButton()
        button.setTitle("Next  →", for: .normal)
        button.backgroundColor = .white
        button.layer.cornerRadius = 10
        button.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2).cgColor
        button.layer.shadowOpacity = 1
        button.layer.shadowRadius = 4
        button.layer.shadowOffset = CGSize(width: 5, height: 5)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 20)
        button.setTitleColor(UIColor.black, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        
        return button
    }()
    
    private var userConsentToTermsOfUseAndPrivacyPolicy: UILabel = {
        let label = UILabel()
        label.backgroundColor = .clear
        label.text = "By clicking <Next>, you agree to the Terms of Use and Privacy Policy"
        label.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.4)
        //        label.font = UIFont(name: "Roboto-Regular", size: 12)
        label.font = UIFont.systemFont(ofSize: 10)
        label.textAlignment = .center
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private var slidePageControl: UIPageControl = {
        let pageControl = UIPageControl()
        pageControl.tintColor = UIColor.flatRed
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        
        return pageControl
    }()
    private var blurEffect: UIBlurEffect = {
        let view = UIBlurEffect(style: .extraLight)
        return view
    }()
    private var vibrancyEffectView: UIVisualEffectView = {
       let blureffect = UIVisualEffectView()
       return blureffect
    }()
    // MARK: - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        panGesture = UIPanGestureRecognizer(target: self, action: #selector(draggedView(_:)))
        viewDrag.isUserInteractionEnabled = true
        viewDrag.addGestureRecognizer(panGesture)
        setupConstraints()
        setupUI()
    }
    
    // MARK: - Private Methods
    
    @objc func draggedView(_ sender:UIPanGestureRecognizer) {
        self.view.bringSubview(toFront: viewDrag)
        let translation = sender.translation(in: self.view)
        let movementCoordinates = (viewDrag.center.x + translation.x)
        if movementCoordinates > 10.0 && movementCoordinates < 402.0 {
            viewDrag.center = CGPoint(x: viewDrag.center.x + translation.x, y: viewDrag.center.y)
            sender.setTranslation(CGPoint.zero, in: self.view)
            backImageViewEffect.snp.updateConstraints { make in
                make.top.left.bottom.equalToSuperview()
                make.width.equalTo(movementCoordinates - 2.0)
            }
        }
    }
    
    private func setupConstraints() {
        view.addSubview(backImageView)
        
        backImageView.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
        }
        
        if slideNumber == 1 || slideNumber == 2 {
            backImageView.addSubview(titleImageView)
            
            titleImageView.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(180)
                make.leading.equalToSuperview().offset(30)
                make.trailing.equalToSuperview().offset(-30)
                make.height.equalTo(350)
            }
        } else {
            backImageView.subviews.forEach({ $0.removeFromSuperview() })
        }
        
        backImageView.addSubview(backImageViewEffect)
        backImageViewEffect.snp.makeConstraints { make in
            make.top.left.bottom.equalToSuperview()
            make.width.equalTo(UIScreen.main.bounds.width)
        }
        
        backImageView.addSubview(titleInfoLabel)
        
        titleInfoLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(30)
            make.trailing.equalToSuperview().offset(-38)
        }
        
        backImageView.addSubview(infoLabel)
        
        infoLabel.snp.makeConstraints { make in
            make.top.equalTo(titleInfoLabel.snp.bottom).offset(16)
            make.leading.equalToSuperview().offset(30)
            make.trailing.equalToSuperview().offset(-38)
        }
        
        backImageView.addSubview(nextButton)
        
        nextButton.snp.makeConstraints { make in
            make.top.equalTo(infoLabel.snp.bottom).offset(30)
            make.leading.equalToSuperview().offset(30)
            make.trailing.equalToSuperview().offset(-30)
            make.height.equalTo(54)
        }
        
        if slideNumber == 0 {
            backImageView.addSubview(userConsentToTermsOfUseAndPrivacyPolicy)
            
            userConsentToTermsOfUseAndPrivacyPolicy.snp.makeConstraints { make in
                make.top.equalTo(nextButton.snp.bottom).offset(30)
                make.centerX.equalToSuperview()
                make.height.equalTo(42)
                make.width.equalTo(200)
                make.bottom.equalToSuperview().offset(-30)
            }
        } else {
            userConsentToTermsOfUseAndPrivacyPolicy.isHidden = true
            backImageView.addSubview(slidePageControl)
            
            slidePageControl.snp.makeConstraints { make in
                make.top.equalTo(nextButton.snp.bottom).offset(32.5)
                make.centerX.equalToSuperview()
                make.bottom.equalToSuperview().offset(-32.5)
            }
        }
        view.addSubview(viewDrag)
        viewDrag.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(12)
            make.centerX.equalToSuperview()
            make.bottom.equalTo(titleInfoLabel.snp.top)
            make.width.equalTo(4)
        }
    }
    
    private func setupUI() {
        view.backgroundColor = .black
        nextButton.addTarget(self, action: #selector(userPressNextButton), for: .touchUpInside)
        
        guard let content = presenter.setupContent(slideNumber: slideNumber) else { return }
        backImageView.image = UIImage(named: content.backImageNameView)?.addFilter()
        titleInfoLabel.text = content.titleLabel
        infoLabel.text = content.infoLabel
        
        if slideNumber == 2 {
            viewDrag.isHidden = false
            backImageViewEffect.isHidden = false
        } else if slideNumber == 3 {
            backImageViewEffect.snp.updateConstraints { make in
                make.top.left.bottom.equalToSuperview()
                make.width.equalTo(UIScreen.main.bounds.width)
            }
            viewDrag.isHidden = false
            backImageViewEffect.isHidden = false
        } else {
            viewDrag.isHidden = true
            backImageViewEffect.isHidden = true
        }
        
        slidePageControl.currentPage = slideNumber
        slidePageControl.numberOfPages = presenter.content.count
    }
    
    @objc private func userPressNextButton() {
        if presenter.content.count - 1 > slideNumber {
            slideNumber += 1
            setupConstraints()
            setupUI()
        } else {
            if PURCHASE_MANAGER.isExpired == false {
                performSegue(withIdentifier: "toQuoteCategoryTableViewController", sender: nil)
            } else {
                performSegue(withIdentifier: "toPaywallVC", sender: nil)
        }
            //performSegue(withIdentifier: "toPaywallVC", sender: nil)
        }
    }
}


extension UIImage {
    func addFilter() -> UIImage {
        let filter = CIFilter(name: "CIPhotoEffectMono")
        // convert UIImage to CIImage and set as input
        let ciInput = CIImage(image: self)
        filter?.setValue(ciInput, forKey: "inputImage")
        // get output CIImage, render as CGImage first to retain proper UIImage scale
        let ciOutput = filter?.outputImage
        let ciContext = CIContext()
        let cgImage = ciContext.createCGImage(ciOutput!, from: (ciOutput?.extent)!)
        //Return the image
        return UIImage(cgImage: cgImage!)
    }
}
