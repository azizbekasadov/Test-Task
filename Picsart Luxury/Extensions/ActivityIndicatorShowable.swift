//
//  ActivityIndicatorShowable.swift
//  Picsart Luxury
//
//  Created by Alex Balukhtsin on 9.10.22.
//  Copyright © 2022 Tribal Media House. All rights reserved.
//

import UIKit.UIActivityIndicatorView

fileprivate let activityIndicatorTag: Int = 1000

protocol ActivityIndicatorShowable where Self: UIViewController {
    func showActivityIndicator(style: UIActivityIndicatorView.Style, activityColor: UIColor)
    func hideActivityIndicator()
}

extension ActivityIndicatorShowable {

    private var activityIndicator: UIActivityIndicatorView {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.tag = activityIndicatorTag
        return activityIndicator
    }
    
    func showActivityIndicator(style: UIActivityIndicatorView.Style,
                               activityColor: UIColor = .white) {
        guard !isDisplayActivityIndicator() else { return }
        let activityIndicator: UIActivityIndicatorView = self.activityIndicator
        activityIndicator.activityIndicatorViewStyle = style
        activityIndicator.color = activityColor.withAlphaComponent(0.6)
        view.addSubview(activityIndicator)
        activityIndicator.snp.makeConstraints { $0.center.equalTo(view.safeAreaLayoutGuide) }
        activityIndicator.startAnimating()
    }
    
    func showCustomActivityIndicator() {
        guard !isDisplayActivityIndicator() else { return }
        let activityIndicator: UIActivityIndicatorView = self.activityIndicator
        if #available(iOS 13.0, *) {
            activityIndicator.activityIndicatorViewStyle = .large
        } else {
            // Fallback on earlier versions
        }
        activityIndicator.color = .black.withAlphaComponent(0.6)
        activityIndicator.layer.cornerRadius = 10
        activityIndicator.backgroundColor = .white
        activityIndicator.layer.shadowOpacity = 0.1
        activityIndicator.layer.shadowRadius = 4
        activityIndicator.layer.shadowColor = UIColor.black.cgColor
        activityIndicator.layer.shadowOffset = CGSize(width: 0, height: 2)
        activityIndicator.layer.shadowPath = UIBezierPath(
          roundedRect: CGRect(x: 0, y: 0, width: 90, height: 90),
          cornerRadius: 10).cgPath
        view.addSubview(activityIndicator)
        activityIndicator.snp.makeConstraints {
            $0.center.equalTo(view.safeAreaLayoutGuide)
            $0.width.height.equalTo(90)
        }
        activityIndicator.startAnimating()
    }
    
    func hideActivityIndicator() {
        guard let activityIndicator = getActivityIndicatorView() else { return }
        activityIndicator.stopAnimating()
        activityIndicator.removeFromSuperview()
    }
    
    private func getActivityIndicatorView() -> UIActivityIndicatorView? {
        view.viewWithTag(activityIndicatorTag) as? UIActivityIndicatorView
    }
    
    private func isDisplayActivityIndicator() -> Bool {
        getActivityIndicatorView() != nil
    }
}
